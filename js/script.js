var blocks = new Array();
var lines = new Array();
var bg1 = Math.random(0,255); 
var bg2 = Math.random(0,255); 
var bg3 = Math.random(0,255); 

function isset (variable) {
    return (typeof variable != 'undefined');
}

function reload_mapping () {
    var i = 0;
    blocks = new Array();
    lines = new Array();

    $( ".draggable" ).each(function(e){
	blocks[$(this).attr('id')] = new Array();
	blocks[$(this).attr('id')]["top"] = $(this).position().top;
	blocks[$(this).attr('id')]["left"] = $(this).position().left;
	blocks[$(this).attr('id')]["center_y"] = $(this).position().top + ($(this).outerHeight()/2);
	blocks[$(this).attr('id')]["center_x"] = $(this).position().left + ($(this).outerWidth()/2);
	
	if(isset($(this).attr('data-parent')) && $(this).hasClass("visible_child")){
	    lines[i] = new Array ();
	    lines[i][0] = $(this).attr('id');
	    lines[i][1] = $(this).attr('data-parent');
	    i++;
	}
    });
}

function get_parent_id (thism) {
    parent = thism.attr("data-parent");
    return parent;
}

var parents = new Array;
var parents_counter = 0;
function get_parents (thism) {
    parents_counter ++;
    if(thism.attr("data-parent") != undefined){
	parents[parents_counter] = thism.attr("data-parent");
	get_parents($("#"+thism.attr("data-parent")));
    }
}


function update_breadcrumb (parents) {
    $("#breadcrumb").empty();
    for(var i=1;i<parents.length;i++){
	$("#breadcrumb").prepend(" <a data-goto='"+parents[i]+"' href='"+window.location.pathname+"#"+parents[i]+"'>"+parents[i]+"</a> > ");
    }
}

function go_to(div_id) {
    thism = $("#"+div_id);
    $(".button").removeClass('close').addClass('open').val('More');
    thism.find(".button").removeClass('open').addClass('close').val('Go Up');

    //parent breadcrumb
    parents = new Array;
    parents_counter = 0;
    get_parents (thism);
    // update the breadcrumb
    update_breadcrumb(parents);
    
    $(".draggable").removeClass('visible_parent visible_child');
    thism.addClass('visible_parent');
    $(".draggable[data-parent='"+div_id+"']").addClass('visible_child');
	
    //postloading
    $(".visible_parent img, .visible_child img").each(function(){
	$(this).attr("src",$(this).attr('data-src')).removeAttr("data-src");
    });

    //modify hash
    window.location.hash = '#'+thism.attr('id');
    
    //update the coordinates
    reload_mapping();
}

function trim (myString)         
{                 
    return myString.replace(/^\s+/g,'').replace(/\s+$/g,'');
}  


$(document).ready(function(){
    //init loading
    reload_mapping();

    //calculate dependencies for buttons + position 
    $( ".draggable" ).each(function(e){
	$(this).css({
	    "top":parseInt($(this).attr("data-top")),
	    "left":parseInt($(this).attr("data-left")),
	    "width":parseInt($(this).attr("data-width")),
	    "height":parseInt($(this).attr("data-height")),
	    "overflowY":$(this).attr("data-overflow")
	});
	
	if($(".draggable[data-parent="+$(this).attr('id')+"]").length > 0)
	{
	    $(this).append('<input type="button" class="button open" value="More" />');
	}
    });

    //detect argument in url
    if(window.location.href.indexOf("#") >0){       
	var url = window.location.href;       
	    /\#([a-zA-Z0-9_ %]+)$/.exec(url);       
	var arguments = RegExp.$1;       
	trim(arguments);       
	arguments = arguments.replace(" ", "_").replace("%20", "_").toLowerCase();       
	console.log(arguments);      
	if($("#"+arguments).length > 0) {
	    go_to(arguments);
	}
    }

    //if clicked on More
    $(document).on("click",".draggable .button.open",function(){
	go_to($(this).parent().attr('id'));
    });

    //if clicked on Go up
    $(document).on("click",".draggable .button.close",function(){
	go_to($(this).parent().attr('data-parent'));
    });

    //if clicked on breadcrumb
    $(document).on("click","#breadcrumb a",function(){
	go_to($(this).attr('data-goto'));
    });


    // when dragndrop blocks, update coordinates
    $( ".draggable" ).draggable({
	drag: function( event, ui ) {
	    blocks[event.target.id]["top"] = ui.position.top;
	    blocks[event.target.id]["left"] = ui.position.left;
	    blocks[event.target.id]["center_y"] = $(this).position().top + ($(this).outerHeight()/2);
	    blocks[event.target.id]["center_x"] = $(this).position().left + ($(this).outerWidth()/2);
	    $(this).find('.admin .x_coord').val($(this).position().top);
	    $(this).find('.admin .y_coord').val($(this).position().left);
	}
    });

    // PROCESSING.JS FUNCTION -> draw her
    function sketchProc(processing) {

	processing.setup = function() {
	    processing.frameRate(25);
	};
	
	processing.draw = function() {
	    processing.size($(window).width(),$(window).height());
	    
	    //if
	    processing.background(230);

	    for(var i=0;i<lines.length;i++){
		processing.line(blocks[lines[i][0]]["center_x"],blocks[lines[i][0]]["center_y"],blocks[lines[i][1]]["center_x"],blocks[lines[i][1]]["center_y"]);
	    } 
	};
    }

    var canvas = document.getElementById("canvas_processing");
    var processingInstance = new Processing(canvas, sketchProc);

});

