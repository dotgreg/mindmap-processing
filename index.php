<!doctype html>
<html lang="us">
<head>
	<meta charset="utf-8">
	<title>vivien website</title>

	<link href="css/smoothness/jquery-ui-1.10.4.custom.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">

	<script src="js/jquery-1.10.min.js"></script>
	<script src="js/jqueryui-1.10.min.js"></script>
	<script src="js/processing-1.4.1.js"></script>
	<script src="js/script.js"></script>

</head>
</html>
<div id="breadcrumb"></div>
<div id="content_wrapper">
  <div id="root" class="draggable visible_parent" data-top="279" data-left="506" >vivien website </div>
  <div id="biography" class="draggable" data-parent="root" data-top="158" data-left="128" data-width="299" data-height="400" data-overflow="scroll">
    <h1>Biography</h1>
    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
  </div>
  <div id="projects" class="draggable" data-parent="root" data-top="181" data-left="821">projects </div>
  <div id="project_1" class="draggable" data-parent="projects" data-top="14" data-left="334">project 1 </div>

  <div id="div_7" class="draggable" data-parent="project_1" data-top="109" data-left="24">
    <img data-src="images/1.png" width="200px" />   
  </div>
  <div id="div_8" class="draggable" data-parent="project_1" data-top="327" data-left="326" data-width="200" data-height="300" data-overflow="scroll">
    <h1>project 1 description</h1>
    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

  </div>
  <div id="div_9" class="draggable" data-parent="project_1" data-top="109" data-left="620">
    <iframe width="560" height="315" src="//www.youtube.com/embed/3Xrv1mFe3-I" frameborder="0" allowfullscreen></iframe>
  </div>

  <div id="project_2" class="draggable" data-parent="projects" data-top="291" data-left="304">project 2 </div>
  <div id="project_3" class="draggable" data-parent="projects" data-top="427" data-left="704">project 3 </div>
  <div id="project_imprimante" class="draggable" data-parent="projects" data-top="489" data-left="225">project imprimante </div>
  <div id="image_imprimante" class="draggable" data-parent="project_imprimante" data-top="109" data-left="24">
    sdfajfkdslkfadsjlksjdak
    <img data-src="images/1.png" width="200px" />   
  </div>
  <div id="explanation_image_imprimante" class="draggable" data-parent="image_imprimante" data-top="200" data-left="300">
    sdfajfkdslkfadsjlksjdak
  </div>


</div>

<canvas id="canvas_processing" width="100%" height="100%" ></canvas>
<!--<canvas  id="canvas_processing_bg" width="100%" height="100%"  data-processing-sources="js/processing.pde"></canvas>-->


<?php if ($_GET['admin'] == "1") :?>
<script>
 $(document).ready(function(){
   $( ".draggable" ).each(function(e){
     $(this).append('<div class="admin"> Top : <input type="text" class="x_coord" value="" /> <br/> Left : <input type="text" class="y_coord" value="" /></div>');
   });
 });
</script>
<?php endif; ?>


</body>
</html>
