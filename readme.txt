##########
# HOW TO
##########


1) Add a new content
##########

<div id="id_of_the_content" class="draggable" data-parent="parent_id_of_the_content" data-top="100" data-left="200" data-width="200" data-height="300" data-overflow="scroll">
     PUT TEXT, IMAGE, VIDEO, HTML HERE
 </div>

REQUIRED ATTRIBUTES
- id : id of the content
- class="draggable" 

POSSIBLE ATTRIBUTES
- data-parent (if not, it means it is a root) : parent of the content
- data-top : y position of the content
- data-left : x position of the content
- data-width : width of the content
- data-height : height of the content
- data-overflow : whats behavior when the content overflows the window ? (scroll | hidden | visible)

2) Add a new picture
##########

Insert a new content, inside the div, insert: 
    
   <data-src="images/1.png" width="200px" /> 

where data-src is your path to the images/1

3) Add a title and a text
##########

Insert a new content then inside the div, put:

    <h1>Title name here</h1>
    <p>Text content here</h1>

4) Add a video from youtube
##########

Insert a new content then inside the div, put the code form the youtube share panel "embed", it should looks like that:
    
    <iframe width="560" height="315" src="//www.youtube.com/embed/3Xrv1mFe3-I" frameborder="0" allowfullscreen></iframe>

5) use my own processing code on the BG
##########

1- uncomment 
   <!--<canvas  id="canvas_processing_bg" width="100%" height="100%"  data-processing-sources="js/processing.pde"></canvas>--> 
   to
   <canvas  id="canvas_processing_bg" width="100%" height="100%"  data-processing-sources="js/processing.pde"></canvas>
2- insert your code inside js/processing.pde

6) Modify the design, colors and font 
##########
Mess around with the file css/style.css!
